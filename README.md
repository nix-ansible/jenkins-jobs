# Jenkins
![coverage report](https://gitlab.com/nix-ansible/jenkins-jobs/badges/master/build.svg)  

# Ansible Role: Jenkins CI

Installs Jenkins CI on Ubuntu servers.

## Role Variables

Available variables are listed below, see `jenkins-jobs/defaults/main.yml`.

The HTTP port for Jenkins' web interfakce.
```yml
    jenkins_http_port: 8080
```

The Jenkins default HTTP url.
```yml
    jenkins_default_url: "http://localhost"
```
Pre defined Terraform version
```yml
terraform_version: 0.8.8
```
Pre defined Terragrunt version
```yml
terragrunt_version: 0.9.6
```
## Example Playbook

```yaml
- name: Install Credentials used by Jobs
  import_tasks: job-credentials.yml

- name: Install Jobs
  import_tasks: jobs.yml

- name: Install Terraform
  import_tasks: terraform.yml

- name: Install Terragrunt
  import_tasks: terragrunt.yml
```

## License

BSD